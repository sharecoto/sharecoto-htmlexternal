# HTMLの外部ファイルを管理するシンプルなクラス

```php
<?php
$scripts = new \Sharecoto\Scripts(array(
    'jquery' => 'https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js'
));
$scripts->offsetSet('underscore.js', 'https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js');
echo $scripts;
// => <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script><script src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>

$scripts->offsetUnset('underscore.js');
```

ほぼ[ArrayObject](http://php.net/manual/ja/class.arrayobject.php)を継承しているだけのものです。

``render()``で、HTML文字列を組み立てて返します。``__toString()``が``render()``を呼んでいるので、単に`echo`でもHTMLを返します。
