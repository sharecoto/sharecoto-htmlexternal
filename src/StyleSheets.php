<?php
namespace Sharecoto;

class StyleSheets extends HtmlExternal
{
    public function render()
    {
        $string = '';
        foreach ($this as $key => $value) {
            $string .= sprintf(
                '<link rel="stylesheet" href="%s">',
                htmlspecialchars($value, ENT_COMPAT | ENT_HTML5 | ENT_QUOTES, 'UTF-8')
            );
        }
        return $string;
    }
}
