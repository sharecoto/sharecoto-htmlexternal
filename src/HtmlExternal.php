<?php
namespace Sharecoto;

class HtmlExternal extends \ArrayObject
{
    public function __toString()
    {
        return $this->render();
    }

    public function remove($key)
    {
        if (array_key_exists($key, $this)) {
            unset($this[$key]);
        }
        return $this;
    }

    public function render()
    {
        $string = '';
        foreach ($this as $key => $value) {
            $string .= sprintf(
                '<script src="%s"></script>',
                htmlspecialchars($value, ENT_COMPAT | ENT_HTML5 | ENT_QUOTES, 'UTF-8')
            );
        }
        return $string;
    }

}
